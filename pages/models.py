from django.db import models


class Page(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    content = models.TextField()
