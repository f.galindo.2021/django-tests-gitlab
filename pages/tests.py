from django.test import TestCase, Client
from .views import Counter


# Create your tests here.

class PruebaCounter(TestCase):

    def test_count1(self):
        counter = Counter()
        self.assertEqual(counter.count, 0)

    def test_count2(self):
        counter = Counter()
        increment = counter.increment()
        self.assertEqual(increment, 1)

    def test_count3(self):
        counter = Counter()
        increment = counter.increment()
        self.assertEqual(increment, 1)


class Prueba(TestCase):

    def __int__(self):
        self.client = Client()

    def test_prueba_get(self):
        response = self.client.get("/")
        status = response.status_code
        self.assertIs(status, 200)
        response = self.client.get("/patata")
        status = response.status_code
        self.assertEqual(status, 404)

    def test_prueba_post(self):
        c = Client()
        response = c.post('/patata', {'content': 'hola'})
        self.assertEqual(response.status_code, 200)
        response = c.get('/patata')
        self.assertEqual(response.status_code, 200)
